<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

use AppBundle\Entity\Place;
use AppBundle\Form\Type\PlaceType;

class PlaceController extends Controller
{
	/**
	 * @Rest\View(serializerGroups={"place"})
	 * @Rest\Get("/places")
	 */
	public function getPlacesAction(Request $request)
	{
		$places = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:Place')
			->findAll();
		/* @var $places Place[] */

		return $places;
	}

	/**
	 * @Rest\View(serializerGroups={"place"})
	 * @Rest\Get("/places/{place_id}")
	 */
	public function getPlaceAction(Request $request)
	{

		$place = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:Place')
				->find($request->get('place_id'));
		/* @var $place Place */

		if(empty($place)) {
			return \FOS\RestBundle\View\View::create(
				['message' => 'Place not found'],
				Response::HTTP_NOT_FOUND
			);
		}

		return $place;
	}

	/**
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"place"})
	 * @Rest\Post("/places")
	 */
	public function postPlacesAction(Request $request)
	{
		$place = new Place();
		$form = $this->createForm(PlaceType::class, $place);

		$form->submit($request->request->all());

		if($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($place);
			$em->flush();
			return $place;
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\View(serializerGroups={"place"})
	 * @Rest\Put("/places/{place_id}")
	 */
	public function updatePlaceAction(Request $request)
	{
		return $this->updatePlace($request, true);
	}

	/**
	 * @Rest\View(serializerGroups={"place"})
	 * @Rest\Patch("/places/{place_id}")
	 */
	public function patchPlaceAction(Request $request)
	{
		return $this->updatePlace($request, false);
	}

	private function updatePlace(Request $request, $clearMissing)
	{
		$place = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:Place')
				->find($request->get('place_id'));
		/* @var $place Place */

		if(empty($place)) {
			return \FOS\RestBundle\View\View::create(
				['message' => 'Place not found'],
				Response::HTTP_NOT_FOUND
			);
		}

		$form = $this->createForm(PlaceType::class, $place);

		$form->submit($request->request->all(), $clearMissing);

		if($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($place);
			$em->flush();
			return $place;
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\View(statusCode=Response::HTTP_NO_CONTENT, serializerGroups={"place"})
	 * @Rest\Delete("/places/{place_id}")
	 */
	public function removePlaceAction(Request $request)
	{
		$em = $this->get('doctrine.orm.entity_manager');
		$place = $em->getRepository('AppBundle:Place')
				->find($request->get('place_id'));
		/* @var $place Place */

		if(!$place) {
			return;
		}

		foreach($place->getPrices() as $price) {
			$em->remove($price);
		}
		$em->remove($place);
		$em->flush();
	}

}
