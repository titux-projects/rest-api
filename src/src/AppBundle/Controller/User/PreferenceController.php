<?php

namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

use AppBundle\Entity\Preference;
use AppBundle\Form\Type\PreferenceType;

class PreferenceController extends Controller
{

	/**
	 * @Rest\View(serializerGroups={"preference"})
	 * @Rest\Get("/users/{user_id}/preferences")
	 */
	public function getPreferencesAction(Request $request)
	{
		$user = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:User')
				->find($request->get('user_id'));
		/* @var $user User */

		if(empty($user)) {
			return $this->userNotFound();
		}

		return $user->getPreferences();
	}

	/**
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"preference"})
	 * @Rest\Post("/users/{user_id}/preferences")
	 */
	public function postPreferencesAction(Request $request)
	{
		$user = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:User')
				->find($request->get('user_id'));
		/* @var $user User */

		if(empty($user)) {
			return $this->userNotFound();
		}

		$preference = new Preference();
		$preference->setUser($user);

		$form = $this->createForm(PreferenceType::class, $preference);

		$form->submit($request->request->all());

		if($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($preference);
			$em->flush();
			return $preference;
		} else {
			return $form;
		};
	}

	private function userNotFound()
	{
		return \FOS\RestBundle\View\View::create(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
	}

}