<?php

namespace AppBundle\Controller\Place;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

use AppBundle\Entity\Price;
use AppBundle\Form\Type\PriceType;

class PriceController extends Controller
{

	/**
	 * @Rest\View(serializerGroups={"price"})
	 * @Rest\Get("/places/{place_id}/prices")
	 */
	public function getPricesAction(Request $request)
	{
		$place = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:Place')
				->find($request->get('place_id'));
		/* @var $place Place */

		if(empty($place)) {
			return $this->placeNotFound();
		}

		return $place->getPrices();
	}

	/**
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"price"})
	 * @Rest\Post("/places/{place_id}/prices")
	 */
	public function postPicesAction(Request $request)
	{
		$place = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:Place')
				->find($request->get('place_id'));
		/* @var $place Place */

		if(empty($place)) {
			return $this->placeNotFound();
		}

		$price = new Price();
		$price->setPlace($place);

		$form = $this->createForm(PriceType::class, $price);

		$form->submit($request->request->all());

		if($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($price);
			$em->flush();
			return $price;
		} else {
			return $form;
		};
	}

	private function placeNotFound()
	{
		return \FOS\RestBundle\View\View::create(['message' => 'Place not found'], Response::HTTP_NOT_FOUND);
	}

}