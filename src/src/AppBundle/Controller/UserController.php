<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

use AppBundle\Form\Type\UserType;
use AppBundle\Entity\User;

class UserController extends Controller
{
	/**
	 * @Rest\View(serializerGroups={"place"})
	 * @Rest\Get("/users/{user_id}/suggestions")
	 */
	public function getUserSuggestionsAction(Request $request)
	{
		$user = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:User')
				->find($request->get('user_id'));
		/* @var $user User */

		if(empty($user)) {
			return $this->userNotFound();
		}

		$suggestions = [];

		$places = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:Place')
				->findAll();
		/* @var $places Place */

		foreach($places as $place) {
			if($user->preferencesMatch($place->getThemes())) {
				$suggestions[] = $place;
			}
		}

		return $suggestions;
	}

	/**
	 * @Rest\View(serializerGroups={"user"})
	 * @Rest\Get("/users")
	 */
	public function getUsersAction(Request $request)
	{
		$users = $this->get('doctrine.orm.entity_manager')
			->getRepository('AppBundle:User')
			->findAll();
		/* @var $users User[] */

		return $users;
	}

	/**
	 * @Rest\View(serializerGroups={"user"})
	 * @Rest\Get("/users/{user_id}")
	 */
	public function getUserAction(Request $request)
	{
		$user = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:User')
				->find($request->get('user_id'));
		/* @var $user User */

		if(empty($user)) {
			return \FOS\RestBundle\View\View::create(
				['message' => 'User not found'],
				Response::HTTP_NOT_FOUND
			);
		}

		return $user;
	}

	/**
	 * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"user"})
	 * @Rest\Post("/users")
	 */
	public function postUsersAction(Request $request)
	{
		$user = new User();
		$form = $this->createForm(UserType::class, $user);

		$form->submit($request->request->all());

		if($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->persist($user);
			$em->flush();
			return $user;
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\View(serializerGroups={"user"})
	 * @Rest\Put("/users/{user_id}")
	 */
	public function updateUserAction(Request $request)
	{
		return $this->updateUser($request, true);
	}

	/**
	 * @Rest\View(serializerGroups={"user"})
	 * @Rest\Patch("/users/{user_id}")
	 */
	public function patchUserAction(Request $request)
	{
		return $this->updateUser($request, false);
	}

	private function updateUser(Request $request)
	{
		$user = $this->get('doctrine.orm.entity_manager')
				->getRepository('AppBundle:User')
				->find($request->get('user_id'));
		/* @var $user User */

		if(empty($user)) {
			return \FOS\RestBundle\View\View::create(
				['message' => 'User not found'],
				Response::HTTP_NOT_FOUND
			);
		}

		$form = $this->createForm(UserType::class, $user);

		$form->submit($request->request->all());

		if($form->isValid()) {
			$em = $this->get('doctrine.orm.entity_manager');
			$em->merge($user);
			$em->flush();
			return $user;
		} else {
			return $form;
		}
	}

	/**
	 * @Rest\View(statusCode=Response::HTTP_NO_CONTENT, serializerGroups={"user"})
	 * @Rest\Delete("/users/{user_id}")
	 */
	public function removeUserAction(Request $request)
	{
		$em = $this->get('doctrine.orm.entity_manager');
		$user = $em->getRepository('AppBundle:User')
				->find($request->get('user_id'));
		/* @var $user User */

		if($user) {
			$em->remove($user);
			$em->flush();
		}
	}

}
